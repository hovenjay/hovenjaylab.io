"use strict";(self.webpackChunkpersonal_website=self.webpackChunkpersonal_website||[]).push([[1116],{1493:(e,s,n)=>{n.r(s),n.d(s,{assets:()=>r,contentTitle:()=>c,default:()=>m,frontMatter:()=>i,metadata:()=>t,toc:()=>h});var l=n(6070),a=n(5710);const i={},c="141. \u73af\u5f62\u94fe\u8868",t={id:"lc-top-100/linked-list-cycle",title:"141. \u73af\u5f62\u94fe\u8868",description:"\u9898\u76ee\u63cf\u8ff0",source:"@site/front-end/03-lc-top-100/025-linked-list-cycle.md",sourceDirName:"03-lc-top-100",slug:"/lc-top-100/linked-list-cycle",permalink:"/fe/lc-top-100/linked-list-cycle",draft:!1,unlisted:!1,tags:[],version:"current",sidebarPosition:25,frontMatter:{},sidebar:"tutorialSidebar",previous:{title:"234. \u56de\u6587\u94fe\u8868",permalink:"/fe/lc-top-100/palindrome-linked-list"},next:{title:"142. \u73af\u5f62\u94fe\u8868 II",permalink:"/fe/lc-top-100/linked-list-cycle-ii"}},r={},h=[{value:"\u9898\u76ee\u63cf\u8ff0",id:"\u9898\u76ee\u63cf\u8ff0",level:2},{value:"\u89e3\u6cd5\u4e00\uff1a\u54c8\u5e0c\u8868",id:"\u89e3\u6cd5\u4e00\u54c8\u5e0c\u8868",level:2},{value:"\u601d\u8def",id:"\u601d\u8def",level:3},{value:"\u5b9e\u73b0",id:"\u5b9e\u73b0",level:3},{value:"\u590d\u6742\u5ea6\u5206\u6790",id:"\u590d\u6742\u5ea6\u5206\u6790",level:3},{value:"\u89e3\u6cd5\u4e8c\uff1a\u6808",id:"\u89e3\u6cd5\u4e8c\u6808",level:2},{value:"\u601d\u8def",id:"\u601d\u8def-1",level:3},{value:"\u5b9e\u73b0",id:"\u5b9e\u73b0-1",level:3},{value:"\u590d\u6742\u5ea6\u5206\u6790",id:"\u590d\u6742\u5ea6\u5206\u6790-1",level:3},{value:"\u89e3\u6cd5\u4e09\uff1a\u5feb\u6162\u6307\u9488",id:"\u89e3\u6cd5\u4e09\u5feb\u6162\u6307\u9488",level:2},{value:"\u601d\u8def",id:"\u601d\u8def-2",level:3},{value:"\u5b9e\u73b0",id:"\u5b9e\u73b0-2",level:3},{value:"\u590d\u6742\u5ea6\u5206\u6790",id:"\u590d\u6742\u5ea6\u5206\u6790-2",level:3}];function d(e){const s={a:"a",annotation:"annotation",code:"code",h1:"h1",h2:"h2",h3:"h3",li:"li",math:"math",mi:"mi",mn:"mn",mo:"mo",mrow:"mrow",p:"p",pre:"pre",semantics:"semantics",span:"span",strong:"strong",ul:"ul",...(0,a.R)(),...e.components};return(0,l.jsxs)(l.Fragment,{children:[(0,l.jsx)(s.h1,{id:"141-\u73af\u5f62\u94fe\u8868",children:"141. \u73af\u5f62\u94fe\u8868"}),"\n",(0,l.jsx)(s.h2,{id:"\u9898\u76ee\u63cf\u8ff0",children:"\u9898\u76ee\u63cf\u8ff0"}),"\n",(0,l.jsxs)(s.p,{children:["\u7ed9\u4f60\u4e00\u4e2a\u94fe\u8868\u7684\u5934\u8282\u70b9 ",(0,l.jsx)(s.code,{children:"head"})," \uff0c\u5224\u65ad\u94fe\u8868\u4e2d\u662f\u5426\u6709\u73af\u3002"]}),"\n",(0,l.jsxs)(s.p,{children:["\u5982\u679c\u94fe\u8868\u4e2d\u6709\u67d0\u4e2a\u8282\u70b9\uff0c\u53ef\u4ee5\u901a\u8fc7\u8fde\u7eed\u8ddf\u8e2a ",(0,l.jsx)(s.code,{children:"next"})," \u6307\u9488\u518d\u6b21\u5230\u8fbe\uff0c\u5219\u94fe\u8868\u4e2d\u5b58\u5728\u73af\u3002 \u4e3a\u4e86\u8868\u793a\u7ed9\u5b9a\u94fe\u8868\u4e2d\u7684\u73af\uff0c\u8bc4\u6d4b\u7cfb\u7edf\u5185\u90e8\u4f7f\u7528\u6574\u6570 ",(0,l.jsx)(s.code,{children:"pos"})," \u6765\u8868\u793a\u94fe\u8868\u5c3e\u8fde\u63a5\u5230\u94fe\u8868\u4e2d\u7684\u4f4d\u7f6e\uff08\u7d22\u5f15\u4ece 0 \u5f00\u59cb\uff09\u3002\u6ce8\u610f\uff1a",(0,l.jsx)(s.code,{children:"pos"})," \u4e0d\u4f5c\u4e3a\u53c2\u6570\u8fdb\u884c\u4f20\u9012 \u3002\u4ec5\u4ec5\u662f\u4e3a\u4e86\u6807\u8bc6\u94fe\u8868\u7684\u5b9e\u9645\u60c5\u51b5\u3002"]}),"\n",(0,l.jsxs)(s.p,{children:["\u5982\u679c\u94fe\u8868\u4e2d\u5b58\u5728\u73af \uff0c\u5219\u8fd4\u56de ",(0,l.jsx)(s.code,{children:"true"})," \u3002 \u5426\u5219\uff0c\u8fd4\u56de ",(0,l.jsx)(s.code,{children:"false"})," \u3002"]}),"\n",(0,l.jsxs)(s.p,{children:[(0,l.jsx)(s.strong,{children:"\u51fa\u5904"}),"\uff1a",(0,l.jsx)(s.a,{href:"https://leetcode-cn.com/problems/linked-list-cycle",children:"LeetCode"})]}),"\n",(0,l.jsx)(s.h2,{id:"\u89e3\u6cd5\u4e00\u54c8\u5e0c\u8868",children:"\u89e3\u6cd5\u4e00\uff1a\u54c8\u5e0c\u8868"}),"\n",(0,l.jsx)(s.h3,{id:"\u601d\u8def",children:"\u601d\u8def"}),"\n",(0,l.jsxs)(s.p,{children:["\u6211\u4eec\u53ef\u4ee5\u4f7f\u7528\u54c8\u5e0c\u8868\u6765\u5224\u65ad\u94fe\u8868\u4e2d\u662f\u5426\u6709\u73af\u3002\u6211\u4eec\u5b9a\u4e49\u4e00\u4e2a\u54c8\u5e0c\u8868 ",(0,l.jsx)(s.code,{children:"set"}),"\uff0c\u7136\u540e\u904d\u5386\u94fe\u8868\uff0c\u6bcf\u6b21\u904d\u5386\u65f6\uff0c\u6211\u4eec\u5224\u65ad\u5f53\u524d\u8282\u70b9\u662f\u5426\u5728\u54c8\u5e0c\u8868\u4e2d\uff0c\u5982\u679c\u5728\uff0c\u5219\u8bf4\u660e\u94fe\u8868\u4e2d\u6709\u73af\uff0c\u8fd4\u56de ",(0,l.jsx)(s.code,{children:"true"}),"\uff1b\u5426\u5219\uff0c\u6211\u4eec\u5c06\u5f53\u524d\u8282\u70b9\u52a0\u5165\u54c8\u5e0c\u8868\u4e2d\u3002\u5f53\u904d\u5386\u7ed3\u675f\u540e\uff0c\u8bf4\u660e\u94fe\u8868\u4e2d\u6ca1\u6709\u73af\uff0c\u8fd4\u56de ",(0,l.jsx)(s.code,{children:"false"}),"\u3002"]}),"\n",(0,l.jsx)(s.h3,{id:"\u5b9e\u73b0",children:"\u5b9e\u73b0"}),"\n",(0,l.jsx)(s.pre,{children:(0,l.jsx)(s.code,{className:"language-typescript",metastring:"showLineNumbers",children:"function hasCycle(head: ListNode | null): boolean {\n  const set = new Set<ListNode>();\n\n  while (head !== null) {\n    if (set.has(head)) {\n      return true;\n    }\n\n    set.add(head);\n    head = head.next;\n  }\n\n  return false;\n}\n"})}),"\n",(0,l.jsx)(s.h3,{id:"\u590d\u6742\u5ea6\u5206\u6790",children:"\u590d\u6742\u5ea6\u5206\u6790"}),"\n",(0,l.jsxs)(s.ul,{children:["\n",(0,l.jsxs)(s.li,{children:["\u65f6\u95f4\u590d\u6742\u5ea6\uff1a",(0,l.jsxs)(s.span,{className:"katex",children:[(0,l.jsx)(s.span,{className:"katex-mathml",children:(0,l.jsx)(s.math,{xmlns:"http://www.w3.org/1998/Math/MathML",children:(0,l.jsxs)(s.semantics,{children:[(0,l.jsxs)(s.mrow,{children:[(0,l.jsx)(s.mi,{children:"O"}),(0,l.jsx)(s.mo,{stretchy:"false",children:"("}),(0,l.jsx)(s.mi,{children:"n"}),(0,l.jsx)(s.mo,{stretchy:"false",children:")"})]}),(0,l.jsx)(s.annotation,{encoding:"application/x-tex",children:"O(n)"})]})})}),(0,l.jsx)(s.span,{className:"katex-html","aria-hidden":"true",children:(0,l.jsxs)(s.span,{className:"base",children:[(0,l.jsx)(s.span,{className:"strut",style:{height:"1em",verticalAlign:"-0.25em"}}),(0,l.jsx)(s.span,{className:"mord mathnormal",style:{marginRight:"0.02778em"},children:"O"}),(0,l.jsx)(s.span,{className:"mopen",children:"("}),(0,l.jsx)(s.span,{className:"mord mathnormal",children:"n"}),(0,l.jsx)(s.span,{className:"mclose",children:")"})]})})]})]}),"\n",(0,l.jsxs)(s.li,{children:["\u7a7a\u95f4\u590d\u6742\u5ea6\uff1a",(0,l.jsxs)(s.span,{className:"katex",children:[(0,l.jsx)(s.span,{className:"katex-mathml",children:(0,l.jsx)(s.math,{xmlns:"http://www.w3.org/1998/Math/MathML",children:(0,l.jsxs)(s.semantics,{children:[(0,l.jsxs)(s.mrow,{children:[(0,l.jsx)(s.mi,{children:"O"}),(0,l.jsx)(s.mo,{stretchy:"false",children:"("}),(0,l.jsx)(s.mi,{children:"n"}),(0,l.jsx)(s.mo,{stretchy:"false",children:")"})]}),(0,l.jsx)(s.annotation,{encoding:"application/x-tex",children:"O(n)"})]})})}),(0,l.jsx)(s.span,{className:"katex-html","aria-hidden":"true",children:(0,l.jsxs)(s.span,{className:"base",children:[(0,l.jsx)(s.span,{className:"strut",style:{height:"1em",verticalAlign:"-0.25em"}}),(0,l.jsx)(s.span,{className:"mord mathnormal",style:{marginRight:"0.02778em"},children:"O"}),(0,l.jsx)(s.span,{className:"mopen",children:"("}),(0,l.jsx)(s.span,{className:"mord mathnormal",children:"n"}),(0,l.jsx)(s.span,{className:"mclose",children:")"})]})})]})]}),"\n"]}),"\n",(0,l.jsx)(s.h2,{id:"\u89e3\u6cd5\u4e8c\u6808",children:"\u89e3\u6cd5\u4e8c\uff1a\u6808"}),"\n",(0,l.jsx)(s.h3,{id:"\u601d\u8def-1",children:"\u601d\u8def"}),"\n",(0,l.jsxs)(s.p,{children:["\u6211\u4eec\u53ef\u4ee5\u4f7f\u7528\u6808\u6765\u5224\u65ad\u94fe\u8868\u4e2d\u662f\u5426\u6709\u73af\u3002\u6211\u4eec\u5b9a\u4e49\u4e00\u4e2a\u6808 ",(0,l.jsx)(s.code,{children:"stack"}),"\uff0c\u7136\u540e\u904d\u5386\u94fe\u8868\uff0c\u6bcf\u6b21\u904d\u5386\u65f6\uff0c\u6211\u4eec\u5c06\u5f53\u524d\u8282\u70b9\u52a0\u5165\u6808\u4e2d\u3002\u5982\u679c\u5f53\u524d\u8282\u70b9\u5df2\u7ecf\u5728\u6808\u4e2d\uff0c\u8bf4\u660e\u94fe\u8868\u4e2d\u6709\u73af\uff0c\u8fd4\u56de ",(0,l.jsx)(s.code,{children:"true"}),"\uff1b\u5426\u5219\uff0c\u6211\u4eec\u7ee7\u7eed\u904d\u5386\u3002\u5f53\u904d\u5386\u7ed3\u675f\u540e\uff0c\u8bf4\u660e\u94fe\u8868\u4e2d\u6ca1\u6709\u73af\uff0c\u8fd4\u56de ",(0,l.jsx)(s.code,{children:"false"}),"\u3002"]}),"\n",(0,l.jsx)(s.h3,{id:"\u5b9e\u73b0-1",children:"\u5b9e\u73b0"}),"\n",(0,l.jsx)(s.pre,{children:(0,l.jsx)(s.code,{className:"language-typescript",metastring:"showLineNumbers",children:"function hasCycle(head: ListNode | null): boolean {\n  const stack: ListNode[] = [];\n\n  while (head !== null) {\n    if (stack.includes(head)) {\n      return true;\n    }\n\n    stack.push(head);\n    head = head.next;\n  }\n\n  return false;\n}\n"})}),"\n",(0,l.jsx)(s.h3,{id:"\u590d\u6742\u5ea6\u5206\u6790-1",children:"\u590d\u6742\u5ea6\u5206\u6790"}),"\n",(0,l.jsxs)(s.ul,{children:["\n",(0,l.jsxs)(s.li,{children:["\u65f6\u95f4\u590d\u6742\u5ea6\uff1a",(0,l.jsxs)(s.span,{className:"katex",children:[(0,l.jsx)(s.span,{className:"katex-mathml",children:(0,l.jsx)(s.math,{xmlns:"http://www.w3.org/1998/Math/MathML",children:(0,l.jsxs)(s.semantics,{children:[(0,l.jsxs)(s.mrow,{children:[(0,l.jsx)(s.mi,{children:"O"}),(0,l.jsx)(s.mo,{stretchy:"false",children:"("}),(0,l.jsx)(s.mi,{children:"n"}),(0,l.jsx)(s.mo,{stretchy:"false",children:")"})]}),(0,l.jsx)(s.annotation,{encoding:"application/x-tex",children:"O(n)"})]})})}),(0,l.jsx)(s.span,{className:"katex-html","aria-hidden":"true",children:(0,l.jsxs)(s.span,{className:"base",children:[(0,l.jsx)(s.span,{className:"strut",style:{height:"1em",verticalAlign:"-0.25em"}}),(0,l.jsx)(s.span,{className:"mord mathnormal",style:{marginRight:"0.02778em"},children:"O"}),(0,l.jsx)(s.span,{className:"mopen",children:"("}),(0,l.jsx)(s.span,{className:"mord mathnormal",children:"n"}),(0,l.jsx)(s.span,{className:"mclose",children:")"})]})})]})]}),"\n",(0,l.jsxs)(s.li,{children:["\u7a7a\u95f4\u590d\u6742\u5ea6\uff1a",(0,l.jsxs)(s.span,{className:"katex",children:[(0,l.jsx)(s.span,{className:"katex-mathml",children:(0,l.jsx)(s.math,{xmlns:"http://www.w3.org/1998/Math/MathML",children:(0,l.jsxs)(s.semantics,{children:[(0,l.jsxs)(s.mrow,{children:[(0,l.jsx)(s.mi,{children:"O"}),(0,l.jsx)(s.mo,{stretchy:"false",children:"("}),(0,l.jsx)(s.mi,{children:"n"}),(0,l.jsx)(s.mo,{stretchy:"false",children:")"})]}),(0,l.jsx)(s.annotation,{encoding:"application/x-tex",children:"O(n)"})]})})}),(0,l.jsx)(s.span,{className:"katex-html","aria-hidden":"true",children:(0,l.jsxs)(s.span,{className:"base",children:[(0,l.jsx)(s.span,{className:"strut",style:{height:"1em",verticalAlign:"-0.25em"}}),(0,l.jsx)(s.span,{className:"mord mathnormal",style:{marginRight:"0.02778em"},children:"O"}),(0,l.jsx)(s.span,{className:"mopen",children:"("}),(0,l.jsx)(s.span,{className:"mord mathnormal",children:"n"}),(0,l.jsx)(s.span,{className:"mclose",children:")"})]})})]})]}),"\n"]}),"\n",(0,l.jsx)(s.h2,{id:"\u89e3\u6cd5\u4e09\u5feb\u6162\u6307\u9488",children:"\u89e3\u6cd5\u4e09\uff1a\u5feb\u6162\u6307\u9488"}),"\n",(0,l.jsx)(s.h3,{id:"\u601d\u8def-2",children:"\u601d\u8def"}),"\n",(0,l.jsxs)(s.p,{children:["\u6211\u4eec\u53ef\u4ee5\u4f7f\u7528\u5feb\u6162\u6307\u9488\u6765\u5224\u65ad\u94fe\u8868\u4e2d\u662f\u5426\u6709\u73af\u3002\u6211\u4eec\u5b9a\u4e49\u4e24\u4e2a\u6307\u9488 ",(0,l.jsx)(s.code,{children:"slow"})," \u548c ",(0,l.jsx)(s.code,{children:"fast"}),"\uff0c\u5206\u522b\u6307\u5411\u94fe\u8868\u7684\u5934\u8282\u70b9 ",(0,l.jsx)(s.code,{children:"head"}),"\u3002\u7136\u540e\u6211\u4eec\u904d\u5386\u94fe\u8868\uff0c\u6bcf\u6b21\u8fed\u4ee3\u65f6\uff0c",(0,l.jsx)(s.code,{children:"slow"})," \u6307\u9488\u5411\u540e\u79fb\u52a8\u4e00\u4e2a\u8282\u70b9\uff0c",(0,l.jsx)(s.code,{children:"fast"})," \u6307\u9488\u5411\u540e\u79fb\u52a8\u4e24\u4e2a\u8282\u70b9\u3002\u5982\u679c\u94fe\u8868\u4e2d\u6709\u73af\uff0c\u90a3\u4e48 ",(0,l.jsx)(s.code,{children:"slow"})," \u548c ",(0,l.jsx)(s.code,{children:"fast"})," \u6307\u9488\u4e00\u5b9a\u4f1a\u5728\u67d0\u4e2a\u8282\u70b9\u76f8\u9047\u3002\u5982\u679c\u94fe\u8868\u4e2d\u6ca1\u6709\u73af\uff0c\u90a3\u4e48 ",(0,l.jsx)(s.code,{children:"fast"})," \u6307\u9488\u4f1a\u5148\u5230\u8fbe\u94fe\u8868\u7684\u5c3e\u8282\u70b9\u3002"]}),"\n",(0,l.jsx)(s.h3,{id:"\u5b9e\u73b0-2",children:"\u5b9e\u73b0"}),"\n",(0,l.jsx)(s.pre,{children:(0,l.jsx)(s.code,{className:"language-typescript",metastring:"showLineNumbers",children:"function hasCycle(head: ListNode | null): boolean {\n  if (head === null || head.next === null) {\n    return false;\n  }\n\n  let slow: ListNode | null = head;\n  let fast: ListNode | null = head.next;\n\n  while (slow !== fast) {\n    if (fast === null || fast.next === null) {\n      return false;\n    }\n\n    slow = slow!.next;\n    fast = fast.next.next;\n  }\n\n  return true;\n}\n"})}),"\n",(0,l.jsx)(s.h3,{id:"\u590d\u6742\u5ea6\u5206\u6790-2",children:"\u590d\u6742\u5ea6\u5206\u6790"}),"\n",(0,l.jsxs)(s.ul,{children:["\n",(0,l.jsxs)(s.li,{children:["\u65f6\u95f4\u590d\u6742\u5ea6\uff1a",(0,l.jsxs)(s.span,{className:"katex",children:[(0,l.jsx)(s.span,{className:"katex-mathml",children:(0,l.jsx)(s.math,{xmlns:"http://www.w3.org/1998/Math/MathML",children:(0,l.jsxs)(s.semantics,{children:[(0,l.jsxs)(s.mrow,{children:[(0,l.jsx)(s.mi,{children:"O"}),(0,l.jsx)(s.mo,{stretchy:"false",children:"("}),(0,l.jsx)(s.mi,{children:"n"}),(0,l.jsx)(s.mo,{stretchy:"false",children:")"})]}),(0,l.jsx)(s.annotation,{encoding:"application/x-tex",children:"O(n)"})]})})}),(0,l.jsx)(s.span,{className:"katex-html","aria-hidden":"true",children:(0,l.jsxs)(s.span,{className:"base",children:[(0,l.jsx)(s.span,{className:"strut",style:{height:"1em",verticalAlign:"-0.25em"}}),(0,l.jsx)(s.span,{className:"mord mathnormal",style:{marginRight:"0.02778em"},children:"O"}),(0,l.jsx)(s.span,{className:"mopen",children:"("}),(0,l.jsx)(s.span,{className:"mord mathnormal",children:"n"}),(0,l.jsx)(s.span,{className:"mclose",children:")"})]})})]})]}),"\n",(0,l.jsxs)(s.li,{children:["\u7a7a\u95f4\u590d\u6742\u5ea6\uff1a",(0,l.jsxs)(s.span,{className:"katex",children:[(0,l.jsx)(s.span,{className:"katex-mathml",children:(0,l.jsx)(s.math,{xmlns:"http://www.w3.org/1998/Math/MathML",children:(0,l.jsxs)(s.semantics,{children:[(0,l.jsxs)(s.mrow,{children:[(0,l.jsx)(s.mi,{children:"O"}),(0,l.jsx)(s.mo,{stretchy:"false",children:"("}),(0,l.jsx)(s.mn,{children:"1"}),(0,l.jsx)(s.mo,{stretchy:"false",children:")"})]}),(0,l.jsx)(s.annotation,{encoding:"application/x-tex",children:"O(1)"})]})})}),(0,l.jsx)(s.span,{className:"katex-html","aria-hidden":"true",children:(0,l.jsxs)(s.span,{className:"base",children:[(0,l.jsx)(s.span,{className:"strut",style:{height:"1em",verticalAlign:"-0.25em"}}),(0,l.jsx)(s.span,{className:"mord mathnormal",style:{marginRight:"0.02778em"},children:"O"}),(0,l.jsx)(s.span,{className:"mopen",children:"("}),(0,l.jsx)(s.span,{className:"mord",children:"1"}),(0,l.jsx)(s.span,{className:"mclose",children:")"})]})})]})]}),"\n"]})]})}function m(e={}){const{wrapper:s}={...(0,a.R)(),...e.components};return s?(0,l.jsx)(s,{...e,children:(0,l.jsx)(d,{...e})}):d(e)}},5710:(e,s,n)=>{n.d(s,{R:()=>c,x:()=>t});var l=n(758);const a={},i=l.createContext(a);function c(e){const s=l.useContext(i);return l.useMemo((function(){return"function"==typeof e?e(s):{...s,...e}}),[s,e])}function t(e){let s;return s=e.disableParentContext?"function"==typeof e.components?e.components(a):e.components||a:c(e.components),l.createElement(i.Provider,{value:s},e.children)}}}]);